def _get_tricky_reversed_word(word: str) -> str:
    inverted_word = ""
    letters = [letter for letter in word if letter.isalpha()]
    for symbol in word:
        inverted_word += letters.pop() if symbol.isalpha() else symbol
    return inverted_word


def tricky_revers(text: str) -> str:
    if not isinstance(text, str):
        raise TypeError(f"unsupported type {type(text)}. Expected 'str'.")
    words = text.split()
    reversed_words = map(_get_tricky_reversed_word, words)
    return " ".join(reversed_words)


if __name__ == "__main__":
    cases = (
        ("", ""),
        ("a", "a"),
        ("ab", "ba"),
        ("asd fgh", "dsa hgf"),
        ("abcd efgh", "dcba hgfe"),
        ("a1bcd efg!h", "d1cba hgf!e")
    )

    for arg, result in cases:
        assert tricky_revers(arg) == result, f"for tricky_revers({arg!r}) got {tricky_revers(arg)!r}, but expected {result!r}"
